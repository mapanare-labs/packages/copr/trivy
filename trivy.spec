%define debug_package %{nil}

Name:           trivy
Version:        0.58.0
Release:        1%{?dist}
Summary:        Scanner for vulnerabilities in container images, file systems, and Git repositories, as well as for configuration issues and hard-coded secrets

License:        ASL 2.0
URL:            https://aquasecurity.github.io/trivy/
Source0:        https://github.com/aquasecurity/%{name}/releases/download/v%{version}/%{name}_%{version}_Linux-64bit.tar.gz

%description
Scanner for vulnerabilities in container images, 
file systems, and Git repositories, as well as for 
configuration issues and hard-coded secrets

%prep
%autosetup -n %{name} -c

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}/usr/bin
install -p -m 755 %{name} %{buildroot}/usr/bin/

%files
%license LICENSE
%doc README.md
/usr/bin/trivy

%changelog
* Mon Dec 09 2024 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 
* Mon Jun 10 2024 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 
* Tue May 28 2024 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 
* Wed Apr 10 2024 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 
* Tue Apr 09 2024 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 
* Sun Mar 03 2024 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 0.49.1

* Thu Dec 28 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 0.48.1

* Wed Nov 15 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 0.47.0

* Wed Jul 26 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 0.44.1

* Wed Jul 26 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 0.43.1

* Fri Jun 30 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 0.43.0

* Mon Jun 19 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 0.42.1

* Mon May 08 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 0.41.0

* Tue Apr 25 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 0.40.0

* Sat Feb 25 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 0.37.3

* Fri Feb 10 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 0.37.2

* Fri Jan 27 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 0.36.1

* Sat Dec 31 2022 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 0.36.0

* Tue Nov 01 2022 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 0.34.0

* Thu Sep 29 2022 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 0.32.1

* Sat Sep 24 2022 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Updated to version 0.32.0

* Sun Sep 11 2022 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Initial RPM
