#!/bin/bash

# Variables
REPO="aquasecurity/trivy"
SPEC_FILE="./trivy.spec"

SCRIPT_EXECUTION_DATE=$(LC_TIME=C date "+%a %b %d %Y")
NEW_CHANGELOG_ENTRY="* $SCRIPT_EXECUTION_DATE $GIT_USER <$GIT_EMAIL>\n- Bumped to version $latest_version"

# Obtener la última versión del paquete desde la API de GitHub
latest_version=$(curl -s -H "Authorization: token $GITHUB_COM_TOKEN" "https://api.github.com/repos/$REPO/releases/latest" | grep '"tag_name":' | sed -E 's/.*"([^"]+)".*/\1/' | tr -d 'v')

# Obtener la versión actual del archivo .spec
current_version=$(awk '/Version:/ {print $2}' "$SPEC_FILE")

# Verificar si la versión ha cambiado
if [ "$latest_version" != "$current_version" ]; then
    echo "Se encontró una nueva versión del paquete: $latest_version"
    
    # Actualizar el archivo .spec con la nueva versión
    sed -i "s/Version:.*/Version:        $latest_version/" "$SPEC_FILE"
    
    echo "La versión en el archivo .spec ha sido actualizada a $latest_version"
    
    # Buscar la sección %changelog en el archivo .spec
    changelog_location=$(awk '/%changelog/ {print NR; exit}' "$SPEC_FILE")
    
    if [ -z "$changelog_location" ]; then
        echo "No se encontró la sección %changelog en el archivo .spec"
        exit 1
    fi
    
    # Agregar una nueva entrada al changelog si no existe
    if ! grep -q "Bumped to version $latest_version" "$SPEC_FILE"; then
        sed -i "${changelog_location}a\\
$NEW_CHANGELOG_ENTRY" "$SPEC_FILE"
        
        echo "Se ha agregado una nueva entrada al changelog."
    else
        echo "La entrada en el changelog ya está actualizada."
    fi
else
    echo "La versión actual del paquete ($current_version) ya está actualizada."
fi